defmodule Bramble.RatingsTest do

  use Bramble.DataCase

  alias Bramble.{Ratings, Rating}

  test "save ratings on valid params" do
    assert {:ok, %Rating{}} =
      Ratings.save(%{name: "Test User", quality: 23})
  end

  test "does not save on invalid params" do
    # quality is more than 100%

    assert {:error, %Ecto.Changeset{}} =
      Ratings.save(%{name: "Test User", quality: 101})
  end
end
