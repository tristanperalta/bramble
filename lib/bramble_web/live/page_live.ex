defmodule BrambleWeb.PageLive do
  use BrambleWeb, :live_view

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, ratings: Bramble.Ratings.list())}
  end

  @impl true
  def handle_event("save", %{"rating" => params}, socket) do
    case Bramble.Ratings.save(params) do
      {:ok, _rating} ->
        {:noreply, assign(socket, ratings: Bramble.Ratings.list())}
      _ ->
        {:noreply, socket}
    end
  end
end
