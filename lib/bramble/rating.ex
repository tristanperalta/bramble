defmodule Bramble.Rating do

  use Ecto.Schema

  import Ecto.Changeset

  schema "ratings" do
    field :name, :string
    field :quality, :integer
  end

  def changeset(rating, params) do
    rating
    |> cast(params, [:name, :quality])
    |> validate_required([:name, :quality])
    |> validate_inclusion(:quality, 0..100)
  end
end
