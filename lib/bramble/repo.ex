defmodule Bramble.Repo do
  use Ecto.Repo,
    otp_app: :bramble,
    adapter: Ecto.Adapters.Postgres
end
