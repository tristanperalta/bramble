defmodule Bramble.Ratings do

  alias Bramble.Repo
  alias Bramble.Rating

  def list() do
    Repo.all(Rating)
  end

  def save(attrs) do
    %Rating{}
    |> Rating.changeset(attrs)
    |> Repo.insert()
  end
end
