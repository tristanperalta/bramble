# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Bramble.Repo.insert!(%Bramble.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

[
  {"Aldo Raine", 8},
  {"Boba Fett", 8},
  {"Frank Futter", 9},
  {"Marsellus Wallace", 25},
  {"Sofie Fatale", 100},
  {"Zed", 50}
] |> Enum.map(fn {name, quality} -> %Bramble.Rating{name: name, quality: quality} end)
  |> Enum.each(&Bramble.Repo.insert! &1)
