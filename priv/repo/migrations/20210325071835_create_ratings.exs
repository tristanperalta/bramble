defmodule Bramble.Repo.Migrations.CreateRatings do
  use Ecto.Migration

  def change do
    create table(:ratings) do
      add :name, :string
      add :quality, :integer
    end
  end
end
