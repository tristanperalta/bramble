# Bramble

To start the server:

```sh
sudo docker-compose up
```

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Development environment

If you are running from `mix phx.server` (without docker) make
make sure to update database hostname in `config/dev.exs` to
`localhost`.